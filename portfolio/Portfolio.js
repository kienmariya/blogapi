const projects = [
  {
    name: "Начало пути ",
    description:
      "Когда я владела только базовыми знаниями в html, css и js я попала на учебную стажировку в компании Noveo. Там после 2ух месячного обучения я с напарником написала свой первый проект: приложение для менеджмента времени. ",
    technology: "ReactJs, Redux, Material-ui, ChartJs",
    img: [
      "http://localhost:8080/time_fix_main.png",
      "http://localhost:8080/time_fix_chatrs.png"
    ]
  },
  {
    name: "Игра для конкурса",
    description:
      "Где-то в апреле 2018го мои брограммисты наткнулись на пост во Вконтакте, где объявлялся конкурс, условиями которого было написание 2D игры на игровом движке Corona. Мы оперативно приступили к написанию. Разделили обязанности. Даже пытались сами написать музычку для игры. Далеко мы правда не продвинулись:)",
    technology: "Corona, Lua",
    img: ["http://localhost:8080/game.jpg"]
  },
  {
    name: "Шифт",
    description:
      "Летом я пошла в летнюю школу ШИФТ(школа информационных и финансовых технологий). Там в течении двух недель мы работали над проектом Eventhub. ",
    technology:
      "ReactJs, Redux, Material-ui, yandex maps api, java(для серверной части)",
    img: ["http://localhost:8080/SFIT.jpg"]
  },
  {
    name: "Почти Skype",
    description:
      "Мне очень захотелось поподробнее узнать про то, что такое p2p соединения и как в реальном времени можно обмениваться информацией в формате видео",
    technology: "WebRTC, socket.io",
    img: ["http://localhost:8080/img.jpg"]
  },
  {
    name: "CouchSurfing",
    description:
      "Я с одногруппником решили поучавстовать в хаккатоне от Naviaddress, где решили разработать приложение для каучсерфинга. В сроки мы, к сожалению, не уложились",
    technology: "ReactJs, Redux, Material-ui, yandex maps api",
    img: [
      "http://localhost:8080/naviapp1.png",
      "http://localhost:8080/naviapp2.png"
    ]
  },
  {
    name: "Подарок на нг",
    description: "Распробовала Canvas на вкус",
    technology: "HTML, CSS, Canvas",
    img: ["http://localhost:8080/newYear.png"]
  }
];

module.exports = projects;
