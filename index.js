const express = require("express");
const app = express();
global.express = express;
global.app = app;
var path = require("path");

var cors = require("cors");
var bodyParser = require("body-parser");
//
var fs = require("fs");
//streams
const through = require("through2");
var map = require("through2-map");

//markdown parser
const marked = require("marked");

app.use(cors());
app.use(bodyParser.json());
// app.use(express.static(__dirname + "/portfolio/img"));
require("./app/routes")();

// const arr = require("./portfolio/Portfolio");
//
// app.use(express.static(__dirname + "/portfolio/img"));
// app.get("/portfolio", (req, res) => {
//   res.send(arr);
// });

// app.get("/blog", function(req, res) {
//   res.header("Content-Type", "text/html; charset=utf-8");
//   let readableStream = fs.createReadStream("read.txt", "utf8");
//   const display = map(function(chunk) {
//     return marked(chunk.toString());
//   });

//   readableStream
//     .pipe(display)
//     .on("end", () => {
//       res.end("\n<!-- End stream -->");
//     })
//     .pipe(res);
// });

// app.get("/blog/aticle", (req, res) => {
//   console.log("111");
// });
// app.get("/blog/article/:article", function(req, res) {
//   console.log("req.params", req.params);
//   let param = req.params.article;
//   console.log(5);
//   console.log("param", param);
//   app.use(express.static(__dirname + `/articles/` + param));
//   let path = `./articles/${param}/article.md`;

//   if (param.indexOf("png") === -1) {
//     let readableStream = fs.createReadStream(path, "utf8");
//     const display = map(function(chunk) {
//       return marked(chunk.toString());
//     });

//     readableStream
//       .pipe(display)
//       .on("end", () => {
//         res.end("\n<!-- End stream -->");
//       })
//       .pipe(res);
//   } else {
//     res.end();
//   }
// });

const PORT = process.env.PORT || 8080;

app.listen(PORT, function () {
  console.log(`Example app listening on port ${PORT}`);
});
