const portfolioController = require("../controllers/portfolioController");
const blogController = require("../controllers/blogController");
const addArticleController = require("../controllers/addArticleController");
const articleController = require("../controllers/articleController");
const authController = require("../controllers/authController");
module.exports = function() {
  app.get("/portfolio", portfolioController);
  app.get("/blog", blogController);
  app.get("/addarticle", addArticleController);
  app.get("/blog/article/:id", articleController);
  app.post("/auth", authController);
};
