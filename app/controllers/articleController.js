const path = require("path");
const fs = require("fs");
const marked = require("marked");
var map = require("through2-map");
module.exports = (req, res) => {
    console.log("req.params", req.params);
    let id = req.params.id;
    console.log("param", id);
    app.use(express.static((path.join(__dirname, `../../articles/${id}`))));

    const articlePath = `./articles/${id}/article.md`;
    let article = fs.createReadStream(articlePath, "utf8");
    const display = map(function (chunk) {
        return marked(chunk.toString());
    });

    article
        .pipe(display)
        .on("end", () => {
            res.end("\n<!-- End stream -->");
        })
        .pipe(res);


    //   if (param.indexOf("png") === -1) {
    //     let readableStream = fs.createReadStream(path, "utf8");
    //     const display = map(function(chunk) {
    //       return marked(chunk.toString());
    //     });

    //     readableStream
    //       .pipe(display)
    //       .on("end", () => {
    //         res.end("\n<!-- End stream -->");
    //       })
    //       .pipe(res);
    //   } else {
    //     res.end();
    //   }
}