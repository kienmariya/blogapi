const projects = require("../../portfolio/Portfolio");

const path = require("path");

module.exports = (req, res) => {
  app.use(express.static((path.join(__dirname, '../../portfolio/img'))));
  res.send(projects);
};
