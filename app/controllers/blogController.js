const mysql = require('mysql');
module.exports = (req, res) => {
    const con = mysql.createConnection({
        host: "localhost",
        user: "root",
        password: "1234"
    });
    con.connect(err => {
        if (err) throw err;
        con.query("use importdb;", (err, result) => {
            if (err) throw err;
        });
        con.query("select * from articles;", (err, result) => {
            if (err) throw err;
            res.send(result);
        });
    });
}