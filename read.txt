
Это мой первый пост моего некудышного блога. Увидит ли это кто-нибудь когда-нибудь, не знаю. В большей степени я делаю это для самой себя.

Для создания блога я использую фреймворк Gatsby.js. GatsbyJS поставляется со своим собственным сервером GraphQL. GraphQL - библиотека,которая описывает как запрашивать данные, и, в основном, используется клиентом для загрузки данных с сервера. Gatsby использует GraphQL для извлечения данных из одного или нескольких источников, таких как локальный диск, WordPress API и т. д.

Такс, начну с начала.
Инструкция по установке Gatsby.js, описанная на офф.сайте [https://www.gatsbyjs.org/docs/](https://www.gatsbyjs.org/docs/):

    npm install --global gatsby-cli
    gatsby new gatsby-site
    cd gatsby-site
    gatsby develop

Открыв IDE/текстовый редактор можно увидеть следующую структуру проекта


![](http://localhost:3001/structure.png)

Просмотрев структуру проекта и немного почитав доки. Я поняла что по адрессу src/pages находятся js файлы, имена которых автоматически становятся именем пути. Т.е. создав файл hello-world.js, заполнив его, и перейдя по адресу http://localhost:8000/hello-world/ на странице отобразится содержимое файла hello-world.js.

gatsby-config.js - файл в котором прописываются установленные плагины. Каждый раз при установке нового плагина нужно обновлять данный файл. На данный момент в файле прописан title сайта и плагин gatsby-plugin-react-helmet, который позволяет изменять head теги.

![](./gatsbyConfig.png)

Потом прочитав несколько инструкций я по шагам выполнила следующие действия:

- Установила плагин npm install gatsby-plugin-catch-links

- Установила плагин npm install gatsby-source-filesystem. Это плагин для получения "исходных" данных из файловой системы. В качестве исходных данных у меня выступают файлы Markdown
  Указала где искать исходные файлы(файл gatsby.config.js):

  ```
  plugins: [
  `gatsby-plugin-react-helmet`,
  `gatsby-plugin-catch-links`,
  {
  resolve: `gatsby-source-filesystem`,
  options: {
  path: `${__dirname}/src/pages`,
  name: 'pages',
  },
  }
  ]
  ```

- npm install gatsby-transformer-remark. Преобразует файлы .md в HTML
  Файл gatsby.config.js теперь выглядит так:

```
    module.exports = {
      siteMetadata: {
     title: `Gatsby Default Starter`,
      },
      plugins: [
     `gatsby-plugin-react-helmet`,
     `gatsby-plugin-catch-links`,
     {
     resolve: `gatsby-source-filesystem`,
     options: {
     path: `${__dirname}/src/pages`,
     name: 'pages',
     },
     }
      ],
    }
```

- Пришло время добавлять новые записи. Плагин gatsby-source-filesystem, ожидает наш контент в src/pages, так что именно туда, мы его и поместим.
  Создала папку src/pages/01-06-18-start и поместила внутрь index.md с таким содержимым:

  ***

> path: "/hello-world"

> date: "2018-06-01T17:12:33.962Z"

> title: "My First Gatsby Post"

---

> Мой первый пост

    Блок, окруженный тире, называется frontmatter. Данные, которые здесь указаны, а также другие данные файла разметки, будут распознаваться gatsby-transformer-remark плагином.

    Поле path из блока окруженного тире будет использоваться для указания пути URL.Например, файл разметки выше будет отображаться localhost:8000/hello-world.

- В папке нужно прописать некий шаблон для отображения каждого файла разметки(поста в блоге) src/templates/blog-post.js

```
    import React from "react";
    import Helmet from "react-helmet";

    export default function Template({
      data
    }) {
      const post = data.markdownRemark;
      return (
     <div className="blog-post-container">
     <Helmet title={`CodeStack - ${post.frontmatter.title}`} />
     <div className="blog-post">
     <h1>{post.frontmatter.title}</h1>
     <div
     className="blog-post-content"
     dangerouslySetInnerHTML={{ __html: post.html }}
     />
     </div>
     </div>
      );
    }
```

Я создала Template компонент для получения data объекта, который будет получен из запроса GraphQL, который нужно написать.

- Еще раз, запрос GraphQL необходим для извлечения данных в компонент. Результат запроса вводится в компонент Template.

Запрос к GraphQL:

```
export const pageQuery = graphql`
  query BlogPostByPath($path: String!) {
 markdownRemark(frontmatter: { path: { eq: $path } }) {
 html
 frontmatter {
 date(formatString: "MMMM DD, YYYY")
 path
 title
 }
 }
  }
`
;
```

В вышеуказанном коде совершается запрос BlogPostByPath с аргументом $path, чтобы вернуть пост в блоге, связанный
с путем, который мы сейчас просматриваем.
Свойство markdownRemark содержит все детали файла разметки

Полностью выглядит так:

```
import React from "react";
import Helmet from "react-helmet";

export default function Template({
data
}) {
const post = data.markdownRemark;
return (

 <div className="blog-post-container">
 <Helmet title={`CodeStack - ${post.frontmatter.title}`} />
 <div className="blog-post">
 <h1>{post.frontmatter.title}</h1>
 <div
 className="blog-post-content"
 dangerouslySetInnerHTML={{ __html: post.html }}
 />
 </div>
 </div>
  );
}

export const pageQuery = graphql`
  query BlogPostByPath($path: String!) {
 markdownRemark(frontmatter: { path: { eq: $path } }) {
 html
 frontmatter {
 date(formatString: "MMMM DD, YYYY")
 path
 title
 }
 }
  }
`
;
```

- Gatsby предоставляет API-интерфейс Node, который обеспечивает функциональность для создания динамических страниц из постов в блогах. (в общем так и не поняла зачем это нужно.
  В документации,как я поняла, написано что это автоматическое созадание страниц)

В файле gatsby-node.js нужно прописать следующий код:

```
const path = require('path');

exports.createPages = ({ boundActionCreators, graphql }) => {
  const { createPage } = boundActionCreators;

const blogPostTemplate = path.resolve(`src/templates/blog-post.js`);

return graphql(`{
 allMarkdownRemark(
 sort: { order: DESC, fields: [frontmatter___date] }
 limit: 1000
 ) {
 edges {
 node {
 excerpt(pruneLength: 250)
 html
 id
 frontmatter {
 date
 path
 title
 }
 }
 }
 }
  }`)
 .then(result => {
 if (result.errors) {
 return Promise.reject(result.errors);
 }

result.data.allMarkdownRemark.edges
 .forEach(({ node }) => {
 createPage({
 path: node.frontmatter.path,
 component: blogPostTemplate,
 context: {} // additional data can be passed via context
 });
 });
 });
}
```

8.Если в блоге много постов, то хочется чтобы на начальной странице выводился спиcок этих постов. Для этого
в файле src/pages/index.js нужно вставить следующий фрагмент кода:

```html
import React from "react";
import Link from "gatsby-link";
import Helmet from "react-helmet";

export default function Index({ data }) {
const { edges: posts } = data.allMarkdownRemark;
return (

 <div className="blog-posts">
 {posts
 .filter(post => post.node.frontmatter.title.length > 0)
 .map(({ node: post }) => {
 return (
 <div className="blog-post-preview" key={post.id}>
 <h1>
 <Link to={post.frontmatter.path}>{post.frontmatter.title}</Link>
 </h1>
 <h2>{post.frontmatter.date}</h2>
 <p>{post.excerpt}</p>
 </div>
 );
 })}
 </div>
  );
}


export const pageQuery = graphql`
  query IndexQuery {
 allMarkdownRemark(sort: { order: DESC, fields: [frontmatter___date] }) {
 edges {
 node {
 excerpt(pruneLength: 250)
 id
 frontmatter {
 title
 date(formatString: "MMMM DD, YYYY")
 path
 }
 }
 }
 }
  }
`;
```

И вот как выглядит результат

![](./posts.png)

Ну а напоследок. Есле не хочется в ручную делать то что я делала выше(пвставлять разнообразные куски кода), то можно упростиьт себе работу. Просто при установки вместо `gatsby new gatsby-site` в терминале нужно написать `gatsby new gatsby-blog https://github.com/gatsbyjs/gatsby-starter-blog`